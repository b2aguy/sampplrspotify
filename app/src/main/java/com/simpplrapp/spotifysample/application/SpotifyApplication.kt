package com.simpplrapp.spotifysample.application

import android.app.Application
import android.content.Context

/**
 * Created by avninder.singh on 22/03/18.
 */
class SpotifyApplication : Application() {

    companion object {

        private lateinit var mInstance: SpotifyApplication

        fun getContext(): Context {
            return mInstance.getApplicationContext()
        }

        @Synchronized
        fun getInstance(): SpotifyApplication? {
            return mInstance
        }
    }


    override fun onCreate() {
        super.onCreate()
        mInstance = this
    }
}
