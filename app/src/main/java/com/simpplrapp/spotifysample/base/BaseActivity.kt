package com.simpplrapp.spotifysample.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Created by avninder.singh on 22/03/18.
 */

abstract class BaseActivity<V : BaseView, T : RxPresenter<V>> : AppCompatActivity() {

    internal abstract val layoutId: Int
    internal abstract fun getExtras()
    internal abstract fun setupData()
    protected abstract val presenter: T
    protected var mPresenter: T? = null

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        mPresenter = presenter
        mPresenter?.attachView(this as V)
        getExtras()
        setupData()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter?.detachView()
    }
}
