package com.simpplrapp.spotifysample.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simpplrapp.spotifysample.interfaces.BaseInteractor
import com.simpplrapp.spotifysample.ui.activities.HomeActivity

/**
 * Created by avninder.singh on 22/03/18.
 */

abstract class BaseFragment<V : BaseView, T : RxPresenter<V>> : Fragment() {

    protected var mPresenter: T? = null

    internal abstract val layoutId: Int
    internal abstract fun getExtras(savedInstanceState: Bundle?)
    internal abstract fun setupData()
    protected abstract val presenter: T

    var mView: View? = null
    var mBaseInteractor: BaseInteractor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter = presenter
        getExtras(savedInstanceState ?: arguments)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (mView == null) {
            mView = inflater.inflate(layoutId, container, false)
        }
        mPresenter?.attachView(this as V)
        return mView
    }

    override fun onDetach() {
        super.onDetach()
        mPresenter?.detachView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupData()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            mBaseInteractor = context as HomeActivity
        } catch (e: Exception) {
            throw RuntimeException("BaseFragment must be only used in HomeActivity")
        }
    }

}