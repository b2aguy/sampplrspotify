package com.simpplrapp.spotifysample.base

/**
 * Created by avninder.singh on 22/03/18.
 */

interface BasePresenter<in T : BaseView> {
    fun attachView(baseView: T)

    fun detachView()
}
