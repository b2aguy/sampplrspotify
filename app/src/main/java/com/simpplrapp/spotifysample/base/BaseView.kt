package com.simpplrapp.spotifysample.base

import com.simpplrapp.spotifysample.exceptions.SpotifyException

/**
 * Created by avninder.singh on 22/03/18.
 */

interface BaseView {
    fun showLoadingState()

    fun hideLoadingState()

    fun showErrorState(e: SpotifyException)

}
