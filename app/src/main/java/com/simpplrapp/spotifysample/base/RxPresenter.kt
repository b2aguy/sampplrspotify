package com.simpplrapp.spotifysample.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by avninder.singh on 22/03/18.
 */
open class RxPresenter<T : BaseView> : BasePresenter<T> {

    protected var mView: T? = null
    protected var mDisposable: CompositeDisposable? = null

    protected fun unSubscribe() {
        mDisposable?.clear()
    }

    protected fun addSubscribe(disposable: Disposable) {
        if (mDisposable == null) {
            mDisposable = CompositeDisposable()
        }
        mDisposable?.add(disposable)
    }


    override fun attachView(baseView: T) {
        this.mView = baseView
    }

    override fun detachView() {
        mView = null
        unSubscribe()
    }
}