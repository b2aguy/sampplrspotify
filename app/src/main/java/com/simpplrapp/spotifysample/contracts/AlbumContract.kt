package com.simpplrapp.spotifysample.contracts

import com.simpplrapp.spotifysample.base.BasePresenter
import com.simpplrapp.spotifysample.base.BaseView
import com.simpplrapp.spotifysample.models.Album

/**
 * Created by avninder.singh on 22/03/18.
 */
interface AlbumContract {

    interface View : BaseView {
        fun showAlbums(albums: ArrayList<Album>)
    }


    interface Presenter : BasePresenter<AlbumContract.View> {
        fun getSavedAlbums(offset : Int)
    }
}