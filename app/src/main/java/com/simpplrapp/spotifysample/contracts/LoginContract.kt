package com.simpplrapp.spotifysample.contracts

import com.simpplrapp.spotifysample.base.BasePresenter
import com.simpplrapp.spotifysample.base.BaseView

/**
 * Created by avninder.singh on 22/03/18.
 */
interface LoginContract {

    interface View : BaseView {
        fun onUserProfileRetrieved()
        fun onAccessTokenReceived()
        fun getCallbackUri() : String
    }

    interface Presenter : BasePresenter<LoginContract.View> {
        fun getUserProfile()
        fun getAccessToken()
        fun refreshAccessToken()
    }
}