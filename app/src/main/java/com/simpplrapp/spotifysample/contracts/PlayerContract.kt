package com.simpplrapp.spotifysample.contracts

import com.simpplrapp.spotifysample.base.BasePresenter
import com.simpplrapp.spotifysample.base.BaseView
import com.simpplrapp.spotifysample.models.Track

/**
 * Created by avninder.singh on 24/03/18.
 */
interface PlayerContract {

    interface View : BaseView {
        fun onNextClicked()
        fun onPreviousClicked()
        fun onShuffleClicked()
        fun onPlay()
        fun onPause()
        fun onRepeat()
        fun onTrackDetailReceived(track : Track)
    }

    interface Presenter : BasePresenter<PlayerContract.View> {
        fun pause()
        fun play()
        fun shuffle()
        fun next()
        fun previous()
        fun repeat()
        fun getTrackDetails(trackID : String)
    }
}