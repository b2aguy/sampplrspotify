package com.simpplrapp.spotifysample.contracts

import com.simpplrapp.spotifysample.base.BasePresenter
import com.simpplrapp.spotifysample.base.BaseView
import com.simpplrapp.spotifysample.models.Track

/**
 * Created by avninder.singh on 22/03/18.
 */
interface TrackContract {

    interface View : BaseView {
        fun showTracks(tracks: ArrayList<Track>)
    }


    interface Presenter : BasePresenter<TrackContract.View> {
        fun getTracksForAlbum(id : String, offset : Int)
    }
}