package com.simpplrapp.spotifysample.data.local

import android.content.Context
import com.simpplrapp.spotifysample.application.SpotifyApplication

/**
 * Created by avninder.singh on 22/03/18.
 */

private const val ACCESS_TOKEN = "access_token"
private const val REFRESH_TOKEN = "refresh_token"
private const val ACCESS_CODE = "access_code"
private const val EMAIL = "email"
private const val DISPLAY_NAME = "name"
private const val PROFILE_PICTURE_URL = "profile_picture_url"

class SharedPrefs {

    //Write Data to File
    private fun writeDataToFile(key: String, data: Any) {
        //Get Editor
        val editor = SHARED_PREFERENCES.edit()

        if (data is Byte || data is Short || data is Int) {
            editor.putInt(key, data as Int)
        } else if (data is Long) {
            editor.putLong(key, data)
        } else if (data is Float || data is Double) {
            editor.putFloat(key, data as Float)
        } else if (data is Boolean) {
            editor.putBoolean(key, data)
        } else if (data is String) {
            editor.putString(key, data)
        }

        //Commit Write
        editor.apply()
    }

    //Read Data from File
    private fun readDataFromFile(key: String, dataClass: Class<*>): Any? {
        return if (dataClass == Byte::class.java || dataClass == Short::class.java || dataClass == Int::class.java) {
            SHARED_PREFERENCES.getInt(key, 0)
        } else if (dataClass == Long::class.java) {
            SHARED_PREFERENCES.getLong(key, 0L)
        } else if (dataClass == Float::class.java || dataClass == Double::class.java) {
            SHARED_PREFERENCES.getFloat(key, 0.00f)
        } else if (dataClass == Boolean::class.java) {
            SHARED_PREFERENCES.getBoolean(key, false)
        } else if (dataClass == String::class.java) {
            SHARED_PREFERENCES.getString(key, "")
        } else {
            null
        }

    }

    fun clearSharedPreference() {
        val editor = SHARED_PREFERENCES.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private val SHARED_PREFERENCES = SpotifyApplication.getContext().getSharedPreferences("simpplrapp", Context.MODE_PRIVATE)
        val instance: SharedPrefs by lazy { Holder.INSTANCE }
    }

    object Holder {
        val INSTANCE = SharedPrefs()
    }

    fun getAccessToken(): String {
        return readDataFromFile(ACCESS_TOKEN, String::class.java) as String
    }

    fun getRefreshToken(): String {
        return readDataFromFile(REFRESH_TOKEN, String::class.java) as String
    }

    fun getAccessCode(): String {
        return readDataFromFile(ACCESS_CODE, String::class.java) as String
    }

    fun getEmail(): String {
        return readDataFromFile(EMAIL, String::class.java) as String
    }

    fun getPictureUrl(): String {
        return readDataFromFile(PROFILE_PICTURE_URL, String::class.java) as String
    }

    fun getName(): String {
        return readDataFromFile(DISPLAY_NAME, String::class.java) as String
    }

    fun writeAccessToken(value: String) {
        writeDataToFile(ACCESS_TOKEN, value)
    }

    fun writeRefreshToken(value: String) {
        writeDataToFile(REFRESH_TOKEN, value)
    }

    fun writeAccessCode(value: String) {
        writeDataToFile(ACCESS_CODE, value)
    }

    fun writeEmail(value: String) {
        writeDataToFile(EMAIL, value)
    }

    fun writePictureUrl(value: String) {
        writeDataToFile(PROFILE_PICTURE_URL, value)
    }

    fun writeName(value: String) {
        writeDataToFile(DISPLAY_NAME, value)
    }
}
