package com.simpplrapp.spotifysample.data.network

import android.util.Base64
import com.readystatesoftware.chuck.ChuckInterceptor
import com.simpplrapp.spotifysample.BuildConfig
import com.simpplrapp.spotifysample.application.SpotifyApplication
import com.simpplrapp.spotifysample.data.local.SharedPrefs
import com.simpplrapp.spotifysample.models.*
import com.simpplrapp.spotifysample.utils.CLIENT_ID
import com.simpplrapp.spotifysample.utils.CLIENT_SECRET
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/**
 * Created by avninder.singh on 22/03/18.
 */

private const val END_POINT: String = "https://api.spotify.com/"
const val ACCOUNT_END_POINT: String = "https://accounts.spotify.com/api/token"

private const val ID_PARAM: String = "id"
private const val LIMIT_PARAM: String = "limit"
private const val OFFSET_PARAM: String = "offset"


object APIService {

    private val headerInterceptor = Interceptor { chain ->
        val original = chain.request()
        val request = original.newBuilder()
        if (original.url().encodedPath()?.contains("api/token", true) == true) {
            val base64Encoded = Base64.encodeToString("$CLIENT_ID:$CLIENT_SECRET".toByteArray(charset("UTF-8")), Base64.NO_WRAP)
            request.addHeader("Authorization", "Basic $base64Encoded")
        } else {
            SharedPrefs.instance.getAccessToken().takeIf { it.isNotEmpty() }
                    ?.run {
                        request.addHeader("Authorization", "Bearer $this")
                    }
        }

        request.method(original.method(), original.body())
        chain.proceed(request.build())
    }

    val api: ApiClient by lazy {

        val builder = OkHttpClient.Builder().readTimeout(300, java.util.concurrent.TimeUnit.SECONDS)
                .writeTimeout(300, java.util.concurrent.TimeUnit.SECONDS)
                .connectTimeout(300, java.util.concurrent.TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(httpLoggingInterceptor)
            builder.addInterceptor(headerInterceptor)
            builder.addInterceptor(ChuckInterceptor(SpotifyApplication.getContext()))
        }


        val retrofitBuilder = Retrofit.Builder().baseUrl(END_POINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build())

        retrofitBuilder.build().create(ApiClient::class.java)
    }

    interface ApiClient {

        @GET("v1/albums/{$ID_PARAM}/tracks")
        fun getTracksForAlbum(@Path(ID_PARAM) id: String, @Query(LIMIT_PARAM) limit: String, @Query(OFFSET_PARAM) offset: String): Observable<TrackListResponseBody>

        @GET("v1/me/albums")
        fun getSavedAlbums(@Query(LIMIT_PARAM) limit: String, @Query(OFFSET_PARAM) offset: String): Observable<AlbumListResponseBody>

        @GET("v1/tracks/{$ID_PARAM}")
        fun getTrackDetails(@Path(ID_PARAM) id: String): Observable<Track>

        @GET("v1/me")
        fun getCurrentUserProfile(): Observable<UserResponseBody>

        @FormUrlEncoded
        @POST
        fun refreshAccessToken(@Url url: String,
                               @Field(GRANT_TYPE) grantToken: String,
                               @Field(REFRESH_TOKEN) refresh_token: String): Observable<GetTokenResponse>

        @FormUrlEncoded
        @POST
        fun getAccessToken(@Url url: String,
                           @Field(GRANT_TYPE) grantToken: String,
                           @Field(CODE) code: String,
                           @Field(REDIRECT_URI) redirect_uri: String): Observable<GetTokenResponse>

    }
}