package com.simpplrapp.spotifysample.data.network.observers

import com.simpplrapp.spotifysample.exceptions.APIException
import com.simpplrapp.spotifysample.models.Response
import io.reactivex.observers.DisposableObserver
import retrofit2.HttpException
import java.io.IOException
import java.net.UnknownHostException

/**
 * Created by avninder.singh on 22/03/18.
 */
abstract class SimpplrApiObserver<T> : DisposableObserver<T>() {

    override fun onNext(t: T) {
        when (t) {
            is Response -> {
                onAPISuccess(t)
            }
            else -> {
                onAPIError(APIException.apiError())
            }
        }
    }

    override fun onError(e: Throwable?) {
        onAPIError(convertToAPIException(e))
    }

    public abstract fun onAPIError(exception: APIException)
    public abstract fun onAPISuccess(t: T)


    private fun convertToAPIException(throwable: Throwable?): APIException {

        return when (throwable) {
            is HttpException -> {
                if (throwable.code() == 504) {
                    APIException.unSatisfiableRequest(throwable.response().raw().request().url().toString(), throwable.response(), null)
                } else {
                    APIException.httpError(throwable.response().raw().request().url().toString(), throwable.response(), null)
                }
            }
            is UnknownHostException -> {
                APIException.offlineError(throwable)
            }
            is IOException -> {
                APIException.networkError(throwable)
            }
            else -> {
                APIException.unexpectedError(throwable)
            }
        }

    }
}