package com.simpplrapp.spotifysample.exceptions

import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

/**
 * Created by avninder.singh on 22/03/18.
 */
open class SpotifyException(val errorMessage: String) : Exception()


class APIException private constructor(errorMessage: String,
                                       val url: String?,
                                       val response: Response<*>?,
                                       val errorType: ErrorType,
                                       exception: Throwable?,
                                       val retrofit: Retrofit?) : SpotifyException(errorMessage) {


    enum class ErrorType {
        NETWORK,
        OFFLINE,
        HTTP,
        UNEXPECTED,
        API_ERROR,
        NO_RESULT
    }

    companion object {

        /**
         * This is the default error message to show for all API related errors.
         */
        const val DEFAULT_ERROR_MESSAGE = "Oops! Something went wrong!"
        const val DEFAULT_OFFLINE_ERROR_MESSAGE = "You don't seem to be connected to the Internet"


        /**
         * When there is no Internet connection
         *
         * @param exception
         * @return
         */
        fun offlineError(exception: IOException): APIException {
            return APIException(DEFAULT_OFFLINE_ERROR_MESSAGE, null, null, ErrorType.OFFLINE, exception, null)
        }

        /**
         * When Http status code is 200, and API is giving some error message.
         *
         * @param baseResult
         * @return
         */
        fun apiError(): APIException {
            return APIException(DEFAULT_ERROR_MESSAGE, null, null, ErrorType.API_ERROR, null, null)
        }


        fun noResultError(): APIException {
            return APIException("No Result", null, null, ErrorType.NO_RESULT, null, null)

        }

        /**
         * if 504 UnsatisfiableRequest error
         *
         * @param url
         * @param response
         * @param retrofit
         * @return
         */


        fun unSatisfiableRequest(url: String, response: Response<*>, retrofit: Retrofit?): APIException {
            return APIException(DEFAULT_OFFLINE_ERROR_MESSAGE, url, response, ErrorType.OFFLINE, null, retrofit)
        }

        /**
         * If a non-200 HTTP status code was received from the server end
         *
         * @param url
         * @param response
         * @param retrofit
         * @return
         */
        fun httpError(url: String, response: Response<*>, retrofit: Retrofit?): APIException {
            return APIException(DEFAULT_ERROR_MESSAGE, url, response, ErrorType.HTTP, null, retrofit)
        }

        /**
         * An error occurred while communicating to the server
         *
         * @param exception
         * @return
         */
        fun networkError(exception: IOException): APIException {
            return APIException(DEFAULT_ERROR_MESSAGE, null, null, ErrorType.NETWORK, exception, null)
        }

        /**
         * This could be anything, like parsing error or internal error while communicating
         *
         * @param exception
         * @return
         */
        fun unexpectedError(exception: Throwable?): APIException {
            return APIException(DEFAULT_ERROR_MESSAGE, null, null, ErrorType.UNEXPECTED, exception, null)
        }
    }
}


