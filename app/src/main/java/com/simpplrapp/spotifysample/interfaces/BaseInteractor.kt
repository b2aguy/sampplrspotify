package com.simpplrapp.spotifysample.interfaces

import android.support.v4.app.Fragment
import com.simpplrapp.spotifysample.models.Album

/**
 * Created by avninder.singh on 24/03/18.
 */
interface BaseInteractor {
    fun onNewFragment(fragment: Fragment)
    fun goToPreviousFragment()
    fun setToolbarTitle(title: String)
    fun setAlbumsInNavMenu(albums: ArrayList<Album>)
    fun setUpHomeIcon()
    fun setUpBackIcon()
}