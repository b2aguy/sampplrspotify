package com.simpplrapp.spotifysample.models

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by avninder.singh on 22/03/18.
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class AlbumInfo(
        @SerializedName(ALBUM) var album: Album? = null,
        @SerializedName(ADDED_AT) var added_at: String? = null
) : Parcelable


@SuppressLint("ParcelCreator")
@Parcelize
data class Album(
        @SerializedName(ID) var id: String? = null,
        @SerializedName(ALBUM_TYPE) var album_type: String = "",
        @SerializedName(NAME) var name: String = "",
        @SerializedName(POPULARITY) var popularity: Int = 0,
        @SerializedName(RELEASE_DATE) var release_date: String = "",
        @SerializedName(IMAGES) var images: ArrayList<Image>? = null,
        @SerializedName(LABEL) var label: String? = null,
        @SerializedName(TRACKS) var tracks: TrackListResponseBody? = null,
        @SerializedName(ARTISTS) var artists: ArrayList<Artist>? = null
) : Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Artist(
        @SerializedName(ID) var id: String? = null,
        @SerializedName(TYPE) var type: String? = null,
        @SerializedName(NAME) var name: String = ""
) : Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Image(
        @SerializedName(HEIGHT) var height: Int? = null,
        @SerializedName(WIDTH) var width: Int? = null,
        @SerializedName(URL) var url: String = ""
) : Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Track(
        @SerializedName(ALBUM) var album: Album? = null,
        @SerializedName(ARTISTS) var artists: ArrayList<Artist>? = null,
        @SerializedName(DURATION_MS) var duration_ms: Long? = null,
        @SerializedName(ID) var id: String = "",
        @SerializedName(TYPE) var type: String? = null,
        @SerializedName(POPULARITY) var popularity: Int = 0,
        @SerializedName(NAME) var name: String = ""
) : Parcelable, Response
