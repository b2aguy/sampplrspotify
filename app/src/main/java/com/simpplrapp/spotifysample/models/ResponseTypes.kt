package com.simpplrapp.spotifysample.models

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by avninder.singh on 22/03/18.
 */

@SuppressLint("ParcelCreator")
@Parcelize
data class AlbumListResponseBody(
        @SerializedName(ITEMS) var items: ArrayList<AlbumInfo>? = null,
        @SerializedName(LIMIT) var limit: String? = null,
        @SerializedName(OFFSET) var offset: String? = null,
        @SerializedName(TOTAL) var total: Int? = null
) : Parcelable, Response


@SuppressLint("ParcelCreator")
@Parcelize
data class TrackListResponseBody(
        @SerializedName(ITEMS) var items: ArrayList<Track>? = null,
        @SerializedName(LIMIT) var limit: String? = null,
        @SerializedName(OFFSET) var offset: String? = null,
        @SerializedName(TOTAL) var total: Int? = null
) : Parcelable, Response


@SuppressLint("ParcelCreator")
@Parcelize
data class UserResponseBody(
        @SerializedName(BIRTHDATE) var birthdate: String? = null,
        @SerializedName(DISPLAY_NAME) var display_name: String? = null,
        @SerializedName(EMAIL) var email: String? = null,
        @SerializedName(ID) var id: String = "",
        @SerializedName(IMAGES) var images: ArrayList<Image>? = null,
        @SerializedName(TYPE) var type: String = "",
        @SerializedName(PRODUCT) var product: String = ""
) : Parcelable, Response

data class GetTokenResponse(@SerializedName(ACCESS_TOKEN) var access_token: String,
                            @SerializedName(REFRESH_TOKEN) var refresh_token: String) : Response

interface Response

