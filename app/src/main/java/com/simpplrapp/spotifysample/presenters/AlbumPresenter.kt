package com.simpplrapp.spotifysample.presenters

import com.simpplrapp.spotifysample.base.RxPresenter
import com.simpplrapp.spotifysample.contracts.AlbumContract
import com.simpplrapp.spotifysample.data.network.APIService
import com.simpplrapp.spotifysample.data.network.observers.SimpplrApiObserver
import com.simpplrapp.spotifysample.exceptions.APIException
import com.simpplrapp.spotifysample.models.Album
import com.simpplrapp.spotifysample.models.AlbumListResponseBody
import com.simpplrapp.spotifysample.utils.TransformUtil

/**
 * Created by avninder.singh on 22/03/18.
 */
class AlbumPresenter : RxPresenter<AlbumContract.View>(), AlbumContract.Presenter {

    override fun getSavedAlbums(offset : Int) {
        val disposableObserver = APIService.api.getSavedAlbums("10", offset.toString())
                .compose(TransformUtil.defaultSchedulers())
                .subscribeWith(object : SimpplrApiObserver<AlbumListResponseBody>() {
                    override fun onComplete() {

                    }

                    override fun onStart() {
                        super.onStart()
                        if (offset == 0) {
                            mView?.showLoadingState()
                        }
                    }

                    override fun onAPIError(exception: APIException) {
                        if (offset == 0) {
                            mView?.showErrorState(exception)
                        }
                    }

                    override fun onAPISuccess(t: AlbumListResponseBody) {
                        val list = ArrayList<Album>()
                        t.items?.forEach {
                            it.album?.let { list.add(it) }
                        }
                        mView?.showAlbums(list)
                        mView?.hideLoadingState()
                    }
                })
        addSubscribe(disposableObserver)
    }
}