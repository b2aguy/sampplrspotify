package com.simpplrapp.spotifysample.presenters

import com.simpplrapp.spotifysample.base.RxPresenter
import com.simpplrapp.spotifysample.contracts.LoginContract
import com.simpplrapp.spotifysample.data.local.SharedPrefs
import com.simpplrapp.spotifysample.data.network.ACCOUNT_END_POINT
import com.simpplrapp.spotifysample.data.network.APIService
import com.simpplrapp.spotifysample.data.network.observers.SimpplrApiObserver
import com.simpplrapp.spotifysample.exceptions.APIException
import com.simpplrapp.spotifysample.models.GetTokenResponse
import com.simpplrapp.spotifysample.models.UserResponseBody
import com.simpplrapp.spotifysample.utils.TransformUtil


/**
 * Created by avninder.singh on 22/03/18.
 */
class LoginPresenter : RxPresenter<LoginContract.View>(), LoginContract.Presenter {


    override fun getAccessToken() {
        addSubscribe(APIService.api.getAccessToken(ACCOUNT_END_POINT, "authorization_code", SharedPrefs.instance.getAccessCode(), mView?.getCallbackUri()
                ?: "")
                .compose(TransformUtil.defaultSchedulers())
                .subscribeWith(object : SimpplrApiObserver<GetTokenResponse>() {
                    override fun onAPIError(exception: APIException) {
                        mView?.showErrorState(exception)
                    }

                    override fun onAPISuccess(t: GetTokenResponse) {
                        SharedPrefs.instance.writeAccessToken(t.access_token)
                        SharedPrefs.instance.writeRefreshToken(t.refresh_token)
                        mView?.onAccessTokenReceived()
                    }

                    override fun onComplete() {

                    }
                }))

    }

    override fun refreshAccessToken() {
        addSubscribe(APIService.api.refreshAccessToken(ACCOUNT_END_POINT,
                "refresh_token", SharedPrefs.instance.getRefreshToken())
                .compose(TransformUtil.defaultSchedulers())
                .subscribeWith(object : SimpplrApiObserver<GetTokenResponse>() {
                    override fun onAPIError(exception: APIException) {
                        mView?.showErrorState(exception)
                    }

                    override fun onAPISuccess(t: GetTokenResponse) {
                        SharedPrefs.instance.writeAccessToken(t.access_token)
                        mView?.onAccessTokenReceived()
                    }

                    override fun onComplete() {

                    }
                }))
    }

    override fun getUserProfile() {
        addSubscribe(APIService.api.getCurrentUserProfile()
                .compose(TransformUtil.defaultSchedulers())
                .subscribeWith(object : SimpplrApiObserver<UserResponseBody>() {

                    override fun onStart() {
                        super.onStart()
                        mView?.showLoadingState()
                    }

                    override fun onAPIError(exception: APIException) {
                        mView?.showErrorState(exception)
                    }

                    override fun onAPISuccess(t: UserResponseBody) {
                        SharedPrefs.instance.writeEmail(t.email ?: "")
                        SharedPrefs.instance.writeName(t.display_name ?: "")
                        SharedPrefs.instance.writePictureUrl(t.images?.takeIf { it.size > 0 }?.get(0)?.url
                                ?: "")
                        mView?.onUserProfileRetrieved()
                        mView?.hideLoadingState()
                    }

                    override fun onComplete() {

                    }
                }))
    }
}