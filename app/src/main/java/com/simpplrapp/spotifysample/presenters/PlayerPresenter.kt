package com.simpplrapp.spotifysample.presenters

import com.simpplrapp.spotifysample.base.RxPresenter
import com.simpplrapp.spotifysample.contracts.PlayerContract
import com.simpplrapp.spotifysample.data.network.APIService
import com.simpplrapp.spotifysample.data.network.observers.SimpplrApiObserver
import com.simpplrapp.spotifysample.exceptions.APIException
import com.simpplrapp.spotifysample.models.Track
import com.simpplrapp.spotifysample.utils.TransformUtil

/**
 * Created by avninder.singh on 24/03/18.
 */
class PlayerPresenter : RxPresenter<PlayerContract.View>(), PlayerContract.Presenter {
    override fun getTrackDetails(trackID: String) {
        addSubscribe(APIService.api.getTrackDetails(trackID)
                .compose(TransformUtil.defaultSchedulers())
                .subscribeWith(object : SimpplrApiObserver<Track>() {
                    override fun onAPIError(exception: APIException) {
                        mView?.showErrorState(exception)
                    }

                    override fun onAPISuccess(t: Track) {
                        mView?.onTrackDetailReceived(t)
                    }

                    override fun onComplete() {

                    }

                }))
    }

    override fun pause() {
        //Todo
    }

    override fun play() {
        //Todo
    }

    override fun shuffle() {
        //Todo
    }

    override fun next() {
        //Todo
    }

    override fun previous() {
        //Todo
    }

    override fun repeat() {
        //Todo
    }
}