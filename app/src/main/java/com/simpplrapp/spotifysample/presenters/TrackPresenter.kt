package com.simpplrapp.spotifysample.presenters

import com.simpplrapp.spotifysample.base.RxPresenter
import com.simpplrapp.spotifysample.contracts.TrackContract
import com.simpplrapp.spotifysample.data.network.APIService
import com.simpplrapp.spotifysample.data.network.observers.SimpplrApiObserver
import com.simpplrapp.spotifysample.exceptions.APIException
import com.simpplrapp.spotifysample.models.TrackListResponseBody
import com.simpplrapp.spotifysample.utils.TransformUtil

/**
 * Created by avninder.singh on 22/03/18.
 */
class TrackPresenter : RxPresenter<TrackContract.View>(), TrackContract.Presenter {

    override fun getTracksForAlbum(id: String, offset : Int) {
        val disposableObserver = APIService.api.getTracksForAlbum(id, "10", offset.toString())
                .compose(TransformUtil.defaultSchedulers())
                .subscribeWith(object : SimpplrApiObserver<TrackListResponseBody>() {

                    override fun onComplete() {

                    }

                    override fun onStart() {
                        super.onStart()
                        if (offset == 0) {
                            mView?.showLoadingState()
                        }
                    }

                    override fun onAPIError(exception: APIException) {
                        if (offset == 0) {
                            mView?.showErrorState(exception)
                        }
                    }

                    override fun onAPISuccess(t: TrackListResponseBody) {
                        t.items?.let { mView?.showTracks(it) }
                        mView?.hideLoadingState()
                    }
                })
        addSubscribe(disposableObserver)
    }
}