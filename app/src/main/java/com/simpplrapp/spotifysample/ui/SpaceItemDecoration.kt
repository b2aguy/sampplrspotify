package com.simpplrapp.spotifysample.ui

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by avninder.singh on 24/03/18.
 */

class SpaceItemDecoration(private val spacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State?) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = spacing
        }
        outRect.bottom = spacing
        outRect.left = spacing
        outRect.right = spacing
    }
}
