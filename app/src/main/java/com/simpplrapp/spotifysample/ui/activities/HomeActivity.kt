package com.simpplrapp.spotifysample.ui.activities

import android.os.Build
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.simpplrapp.spotifysample.R
import com.simpplrapp.spotifysample.base.BaseActivity
import com.simpplrapp.spotifysample.base.BaseView
import com.simpplrapp.spotifysample.base.RxPresenter
import com.simpplrapp.spotifysample.data.local.SharedPrefs
import com.simpplrapp.spotifysample.exceptions.SpotifyException
import com.simpplrapp.spotifysample.interfaces.BaseInteractor
import com.simpplrapp.spotifysample.models.Album
import com.simpplrapp.spotifysample.ui.fragments.AlbumFragment
import com.simpplrapp.spotifysample.ui.fragments.TrackListFragment
import com.simpplrapp.spotifysample.utils.dpResourceToPx
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.nav_header_home.view.*

class HomeActivity : BaseActivity<BaseView, RxPresenter<BaseView>>(), BaseView, NavigationView.OnNavigationItemSelectedListener, BaseInteractor {

    override fun setUpHomeIcon() {
        toggle?.isDrawerIndicatorEnabled = true
        toggle?.syncState()
    }

    override fun setUpBackIcon() {
        toggle?.isDrawerIndicatorEnabled = false
        toggle?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp)
        toggle?.syncState()
    }

    private val GROUP_ID = 101
    private val HOME_ITEM_ID = -10
    private val map = HashMap<Int, Album>()
    private var toggle: ActionBarDrawerToggle? = null

    override fun setAlbumsInNavMenu(albums: ArrayList<Album>) {
        val menu = nav_view.menu
        menu.removeGroup(GROUP_ID)
        map.clear()
        val submenu = menu.addSubMenu(GROUP_ID, 0, 0, "Saved Albums")
        albums.forEachIndexed { index, album ->
            map[index] = album
            submenu.add(0, index, 0, album.name)
        }
    }

    override fun setToolbarTitle(title: String) {
        toolbar?.title = title
    }

    override fun showLoadingState() {

    }

    override fun hideLoadingState() {

    }

    override fun showErrorState(e: SpotifyException) {

    }

    override val layoutId: Int
        get() = R.layout.activity_home

    override fun getExtras() {

    }

    override fun setupData() {
        setSupportActionBar(toolbar)



        toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
                .apply {
                    drawer_layout.addDrawerListener(this)
                    syncState()
                    setToolbarNavigationClickListener {
                        goToPreviousFragment()
                    }
                }

        nav_view.menu.add(0, HOME_ITEM_ID, 0, "Home")
        nav_view.setNavigationItemSelectedListener(this)
        val headerView = nav_view.getHeaderView(0)
        headerView.name.text = SharedPrefs.instance.getName().run {
            if (this.isEmpty()) "Not Available" else this
        }
        headerView.email.text = SharedPrefs.instance.getEmail()
        Glide.with(this)
                .load(SharedPrefs.instance.getPictureUrl())
                .override(dpResourceToPx(this, R.dimen.nav_header_dp_width), dpResourceToPx(this, R.dimen.nav_header_dp_width))
                .centerCrop()
                .placeholder(R.drawable.person)
                .into(headerView.imageView)

        showFragment(AlbumFragment(), false)
    }

    override val presenter: RxPresenter<BaseView>
        get() = RxPresenter()


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            HOME_ITEM_ID -> {
                goToRootFragment()
            }
            else -> {
                map[item.itemId]?.let {
                    onNewFragment(TrackListFragment.getInstance(it))
                }
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun goToRootFragment() {
        while (supportFragmentManager.popBackStackImmediate()) {

        }
    }

    private fun showFragment(fragment: Fragment) {
        showFragment(fragment, true)
    }

    private fun showFragment(fragment: Fragment, addToBackStack: Boolean) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (isDestroyed) return
        }
        if (isFinishing) return

        val tr = supportFragmentManager.beginTransaction()
        tr.replace(R.id.container, fragment)
        if (addToBackStack) tr.addToBackStack(fragment.tag)
        tr.commitAllowingStateLoss()
        supportFragmentManager.executePendingTransactions()
    }

    override fun onNewFragment(fragment: Fragment) {
        showFragment(fragment)
    }

    override fun goToPreviousFragment() {
        supportFragmentManager.popBackStackImmediate()
    }
}
