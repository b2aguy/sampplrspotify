package com.simpplrapp.spotifysample.ui.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.simpplrapp.spotifysample.R
import com.simpplrapp.spotifysample.base.BaseActivity
import com.simpplrapp.spotifysample.contracts.LoginContract
import com.simpplrapp.spotifysample.data.local.SharedPrefs
import com.simpplrapp.spotifysample.exceptions.SpotifyException
import com.simpplrapp.spotifysample.presenters.LoginPresenter
import com.simpplrapp.spotifysample.utils.CLIENT_ID
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by avninder.singh on 22/03/18.
 */
class LoginActivity : BaseActivity<LoginContract.View, LoginPresenter>(), LoginContract.View {

    override fun onAccessTokenReceived() {
        if (SharedPrefs.instance.getEmail().isEmpty()) {
            mPresenter?.getUserProfile()
        } else {
            onUserProfileRetrieved()
        }
    }

    override fun getCallbackUri(): String {
        return getRedirectUri().toString()
    }

    private val AUTH_CODE_REQUEST_CODE = 0x10

    override fun showLoadingState() {
        btn_container.visibility = View.VISIBLE
        connect_spotify_btn.visibility = View.INVISIBLE
        progress_bar.visibility = View.VISIBLE
    }

    override fun hideLoadingState() {
        btn_container.visibility = View.VISIBLE
        connect_spotify_btn.visibility = View.VISIBLE
        progress_bar.visibility = View.INVISIBLE
    }

    override fun showErrorState(e: SpotifyException) {
        Toast.makeText(this@LoginActivity, e.errorMessage, Toast.LENGTH_SHORT).show()
        btn_container.visibility = View.VISIBLE
        connect_spotify_btn.visibility = View.VISIBLE
        progress_bar.visibility = View.INVISIBLE
    }

    override fun onUserProfileRetrieved() {
        goToHomeAndFinish()
    }

    override val layoutId: Int
        get() = R.layout.activity_login

    override fun getExtras() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    override fun setupData() {
        btn_container.visibility = View.GONE

        if (SharedPrefs.instance.getAccessCode().isEmpty()) {
            btn_container.visibility = View.VISIBLE
        } else if (SharedPrefs.instance.getAccessToken().isEmpty()) {
            mPresenter?.getAccessToken()
        } else {
            mPresenter?.refreshAccessToken()
        }

        connect_spotify_btn.setOnClickListener {
            requestAccessCode()
        }

    }

    private fun goToHomeAndFinish() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    override val presenter: LoginPresenter
        get() = LoginPresenter()


    private fun requestAccessCode() {
        showLoadingState()
        val request = getAuthenticationRequest(AuthenticationResponse.Type.CODE)
        AuthenticationClient.openLoginActivity(this, AUTH_CODE_REQUEST_CODE, request)
    }


    private fun getAuthenticationRequest(type: AuthenticationResponse.Type): AuthenticationRequest {
        return AuthenticationRequest.Builder(CLIENT_ID, type, getRedirectUri().toString())
                .setShowDialog(true)
                .setScopes(arrayOf("user-read-email", "user-library-read"))
                .build()
    }


    private fun getRedirectUri(): Uri {
        return Uri.Builder()
                .scheme(getString(R.string.com_spotify_sdk_redirect_scheme))
                .authority(getString(R.string.com_spotify_sdk_redirect_host))
                .build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTH_CODE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            AuthenticationClient.getResponse(resultCode, data).code
                    ?.takeIf { it.isNotEmpty() }
                    ?.run {
                        SharedPrefs.instance.writeAccessCode(this)
                        mPresenter?.getAccessToken()
                        return
                    }
        }

        hideLoadingState()
    }
}