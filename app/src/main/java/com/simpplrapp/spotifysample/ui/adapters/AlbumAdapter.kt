package com.simpplrapp.spotifysample.ui.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.simpplrapp.spotifysample.R
import com.simpplrapp.spotifysample.interfaces.BaseInteractor
import com.simpplrapp.spotifysample.models.Album
import com.simpplrapp.spotifysample.ui.fragments.TrackListFragment
import com.simpplrapp.spotifysample.utils.dpResourceToPx
import com.simpplrapp.spotifysample.utils.listen
import kotlinx.android.synthetic.main.item_album.view.*

/**
 * Created by avninder.singh on 24/03/18.
 */
class AlbumAdapter(private val list: ArrayList<Album>, val baseInteractor: BaseInteractor? = null) : RecyclerView.Adapter<AlbumAdapter.Companion.AlbumViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        return AlbumViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_album, null))
                .listen { position, type ->
                    if (position != -1) {
                        baseInteractor?.onNewFragment(TrackListFragment.getInstance(list[position]))
                    }
                }
    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        val album = list[position]
        holder.bindAlbum(album)
    }

    companion object {

        class AlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            @SuppressLint("SetTextI18n")
            fun bindAlbum(album: Album) {
                itemView.album_title.text = album.name
                val stringBuilder = StringBuilder()
                stringBuilder.append("Artists: ")
                val size = album.artists?.size ?: 0
                album.artists?.forEachIndexed { index, artist -> stringBuilder.append(artist.name + (if (index == size - 1) "" else ", ")) }
                itemView.artists.text = stringBuilder.toString()
                itemView.label.text = "Label: ${album.label}"
                itemView.popularity.text = album.popularity.toString()
                Glide.with(itemView.album_cover.context)
                        .load(album.images?.takeIf { it.size > 0 }?.get(0)?.url ?: "")
                        .override(dpResourceToPx(itemView.album_cover.context, R.dimen.album_cover_width), dpResourceToPx(itemView.album_cover.context, R.dimen.album_cover_height))
                        .placeholder(R.drawable.placeholder_cover)
                        .centerCrop()
                        .into(itemView.album_cover)
            }
        }
    }

}