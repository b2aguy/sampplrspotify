package com.simpplrapp.spotifysample.ui.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simpplrapp.spotifysample.R
import com.simpplrapp.spotifysample.interfaces.BaseInteractor
import com.simpplrapp.spotifysample.models.Album
import com.simpplrapp.spotifysample.models.Track
import com.simpplrapp.spotifysample.ui.fragments.PlayerFragment
import com.simpplrapp.spotifysample.utils.listen
import kotlinx.android.synthetic.main.item_text_count.view.*
import kotlinx.android.synthetic.main.item_track.view.*
import java.util.concurrent.TimeUnit


/**
 * Created by avninder.singh on 24/03/18.
 */
class TrackAdapter(private val list: ArrayList<Track>, val album: Album, val baseInteractor: BaseInteractor?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_album -> {
                AlbumAdapter.Companion.AlbumViewHolder(LayoutInflater.from(parent.context).inflate(viewType, null))
            }
            R.layout.item_text_count -> {
                CountViewHolder(LayoutInflater.from(parent.context).inflate(viewType, null))
            }
            else -> {
                TrackViewHolder(LayoutInflater.from(parent.context).inflate(viewType, null)).listen { position, type ->
                    if (position != -1) {
                        baseInteractor?.onNewFragment(PlayerFragment.getInstance(list[position - 2]))
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> {
                R.layout.item_album
            }
            1 -> {
                R.layout.item_text_count
            }
            else -> {
                R.layout.item_track
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size + 2
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CountViewHolder -> {
                holder.bindTrack(album.tracks?.total ?: 0)
            }
            is AlbumAdapter.Companion.AlbumViewHolder -> {
                holder.bindAlbum(album)
            }
            else -> {
                (holder as TrackViewHolder).bindTrack(list[position - 2])
            }
        }
    }

    companion object {

        class TrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            @SuppressLint("SetTextI18n")
            fun bindTrack(track: Track) {
                itemView.track_title.text = track.name
                val stringBuilder = StringBuilder()
                stringBuilder.append("Artists: ")
                val size = track.artists?.size ?: 0
                track.artists?.forEachIndexed { index, artist -> stringBuilder.append(artist.name + (if (index == size - 1) "" else ", ")) }
                itemView.track_artists.text = stringBuilder.toString()

                track.duration_ms?.takeIf { it > 0 }?.run {
                    val minutes = TimeUnit.MILLISECONDS.toMinutes(this)
                    val seconds = TimeUnit.MILLISECONDS.toSeconds(this) - minutes * 60
                    itemView.duration.text = "Duration: $minutes:$seconds"
                }
            }
        }

        class CountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bindTrack(count: Int) {
                itemView.track_count.text = itemView.context.resources.getQuantityString(R.plurals.numberOfTracksAvailable, count, count)
            }
        }
    }

}