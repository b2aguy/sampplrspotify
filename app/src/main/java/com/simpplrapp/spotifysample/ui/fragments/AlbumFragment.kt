package com.simpplrapp.spotifysample.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.simpplrapp.spotifysample.R
import com.simpplrapp.spotifysample.base.BaseFragment
import com.simpplrapp.spotifysample.contracts.AlbumContract
import com.simpplrapp.spotifysample.exceptions.SpotifyException
import com.simpplrapp.spotifysample.models.Album
import com.simpplrapp.spotifysample.presenters.AlbumPresenter
import com.simpplrapp.spotifysample.ui.SpaceItemDecoration
import com.simpplrapp.spotifysample.ui.adapters.AlbumAdapter
import com.simpplrapp.spotifysample.ui.widgets.ProgressLayout
import com.simpplrapp.spotifysample.utils.dpResourceToPx
import kotlinx.android.synthetic.main.fragment_list_data.*

/**
 * Created by avninder.singh on 22/03/18.
 */
class AlbumFragment : BaseFragment<AlbumContract.View, AlbumPresenter>(), AlbumContract.View {

    private val KEY_DATA: String = "KEY_DATA"
    private var albumAdapter: AlbumAdapter? = null
    private val listItems = ArrayList<Album>()


    override fun showLoadingState() {
        progress_layout?.showProgress(true)
    }

    override fun hideLoadingState() {
        progress_layout?.showProgress(false)
    }

    override fun showErrorState(e: SpotifyException) {
        progress_layout?.showError(e.errorMessage)
    }

    override fun showAlbums(albums: ArrayList<Album>) {
        albumAdapter?.let {
            if (albums.isNotEmpty()) {

                if (listItems.size == 0) {
                    mBaseInteractor?.setAlbumsInNavMenu(albums)
                }

                listItems.addAll(albums)
                it.notifyItemRangeInserted(listItems.size - albums.size, albums.size)
            }
        }
    }

    override val layoutId: Int
        get() = R.layout.fragment_list_data


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(KEY_DATA, listItems)
    }

    override fun getExtras(savedInstanceState: Bundle?) {
        savedInstanceState?.getParcelableArrayList<Album>(KEY_DATA)?.run {
            listItems.addAll(this)
        }
    }

    override fun setupData() {
        progress_layout.setProgressListener(object : ProgressLayout.ProgressListener {
            override fun onProgressReloadClick() {
                mPresenter?.getSavedAlbums(listItems.size)
            }

        })
        context?.run {
            recycler_view.layoutManager = LinearLayoutManager(this)
            if (recycler_view.itemDecorationCount == 0) {
                recycler_view.addItemDecoration(SpaceItemDecoration(dpResourceToPx(this, R.dimen.medium_spacing)))
            }
        }
        recycler_view.setEndlessScrollListener {
            mPresenter?.getSavedAlbums(listItems.size)
        }
        albumAdapter = AlbumAdapter(listItems, mBaseInteractor)
        recycler_view.adapter = albumAdapter

        if (listItems.isEmpty()) {
            mPresenter?.getSavedAlbums(listItems.size)
        }
    }

    override fun onResume() {
        super.onResume()
        mBaseInteractor?.setToolbarTitle("Saved Albums")
        mBaseInteractor?.setUpHomeIcon()
    }

    override val presenter: AlbumPresenter
        get() = AlbumPresenter()
}