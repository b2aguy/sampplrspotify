package com.simpplrapp.spotifysample.ui.fragments

import android.os.Bundle
import com.bumptech.glide.Glide
import com.simpplrapp.spotifysample.R
import com.simpplrapp.spotifysample.base.BaseFragment
import com.simpplrapp.spotifysample.contracts.PlayerContract
import com.simpplrapp.spotifysample.exceptions.SpotifyException
import com.simpplrapp.spotifysample.models.Track
import com.simpplrapp.spotifysample.presenters.PlayerPresenter
import kotlinx.android.synthetic.main.fragment_track_player.*

/**
 * Created by avninder.singh on 24/03/18.
 */
class PlayerFragment : BaseFragment<PlayerContract.View, PlayerPresenter>(), PlayerContract.View {

    override fun onTrackDetailReceived(track: Track) {
        this.track = track
        bindView()
    }

    private var track: Track? = null

    companion object {
        fun getInstance(track: Track): PlayerFragment {
            return PlayerFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("track", track)
                }
            }
        }
    }

    override fun showLoadingState() {
        //Todo
    }

    override fun hideLoadingState() {
        //Todo
    }

    override fun showErrorState(e: SpotifyException) {
        //Todo
    }

    override fun onNextClicked() {
        //Todo
    }

    override fun onPreviousClicked() {
        //Todo
    }

    override fun onShuffleClicked() {
        //Todo
    }

    override fun onPlay() {
        //Todo
    }

    override fun onRepeat() {
        //Todo
    }

    override val layoutId: Int
        get() = R.layout.fragment_track_player

    override fun getExtras(savedInstanceState: Bundle?) {
        track = savedInstanceState?.getParcelable<Track>("track")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable("track", track)
    }

    override fun setupData() {
        mPresenter?.getTrackDetails(track?.id ?: "")
        bindView()
    }

    private fun bindView() {
        track?.let {

            Glide.with(this)
                    .load(it.album?.images?.takeIf { it.size > 0 }?.get(0)?.url)
                    .placeholder(R.drawable.placeholder_cover)
                    .override(400, 400)
                    .into(cover_image_view)

            player_track_title.text = it.name
            player_track_artist.text = it.artists?.takeIf { it.size > 0 }?.get(0)?.name ?: ""
        }

    }

    override val presenter: PlayerPresenter
        get() = PlayerPresenter()

    override fun onResume() {
        super.onResume()
        mBaseInteractor?.setToolbarTitle("")
        mBaseInteractor?.setUpBackIcon()
    }

}