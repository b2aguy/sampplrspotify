package com.simpplrapp.spotifysample.ui.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.simpplrapp.spotifysample.R
import com.simpplrapp.spotifysample.base.BaseFragment
import com.simpplrapp.spotifysample.contracts.TrackContract
import com.simpplrapp.spotifysample.exceptions.SpotifyException
import com.simpplrapp.spotifysample.models.Album
import com.simpplrapp.spotifysample.models.Track
import com.simpplrapp.spotifysample.presenters.TrackPresenter
import com.simpplrapp.spotifysample.ui.SpaceItemDecoration
import com.simpplrapp.spotifysample.ui.adapters.TrackAdapter
import com.simpplrapp.spotifysample.ui.widgets.ProgressLayout
import com.simpplrapp.spotifysample.utils.dpResourceToPx
import kotlinx.android.synthetic.main.fragment_track_list.*

/**
 * Created by avninder.singh on 22/03/18.
 */
class TrackListFragment : BaseFragment<TrackContract.View, TrackPresenter>(), TrackContract.View {

    private val KEY_DATA: String = "KEY_DATA"
    private val KEY_ALBUM: String = "KEY_ID"
    private var album: Album? = null
    private var adapter: TrackAdapter? = null
    private val listItems = ArrayList<Track>()

    companion object {
        fun getInstance(album: Album): TrackListFragment {
            return TrackListFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(KEY_ALBUM, album)
                }
            }
        }
    }

    override fun showLoadingState() {
        progress_layout_tracks?.showProgress(true)
    }

    override fun hideLoadingState() {
        progress_layout_tracks?.showProgress(false)
    }

    override fun showErrorState(e: SpotifyException) {
        progress_layout_tracks?.showError(e.errorMessage)
    }

    override fun showTracks(tracks: ArrayList<Track>) {
        adapter?.let {
            if (tracks.isNotEmpty()) {
                listItems.addAll(tracks)
                it.notifyItemRangeInserted(2 + listItems.size - tracks.size, tracks.size)
            }
        }
    }

    override val layoutId: Int
        get() = R.layout.fragment_track_list


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(KEY_DATA, listItems)
    }

    override fun getExtras(savedInstanceState: Bundle?) {
        savedInstanceState?.getParcelableArrayList<Track>(KEY_DATA)?.run {
            listItems.addAll(this)
        }
        savedInstanceState?.getParcelable<Album>(KEY_ALBUM)?.run {
            album = this
        }
    }

    override fun setupData() {
        album?.let {
            progress_layout_tracks.setProgressListener(object : ProgressLayout.ProgressListener {
                override fun onProgressReloadClick() {
                    mPresenter?.getTracksForAlbum(it.id ?: "", listItems.size)
                }

            })
            context?.run {
                recycler_view_tracks.layoutManager = LinearLayoutManager(this)
                if (recycler_view_tracks.itemDecorationCount == 0) {
                    recycler_view_tracks.addItemDecoration(SpaceItemDecoration(dpResourceToPx(this, R.dimen.medium_spacing)))
                }
            }
            recycler_view_tracks.setEndlessScrollListener {
                mPresenter?.getTracksForAlbum(it.id ?: "", listItems.size)
            }
            adapter = TrackAdapter(listItems, it, baseInteractor = mBaseInteractor)
            recycler_view_tracks.adapter = adapter

            if (listItems.isEmpty()) {
                mPresenter?.getTracksForAlbum(it.id ?: "", listItems.size)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        mBaseInteractor?.setToolbarTitle(album?.name ?: "Tracks")
        mBaseInteractor?.setUpBackIcon()
    }

    override val presenter: TrackPresenter
        get() = TrackPresenter()
}