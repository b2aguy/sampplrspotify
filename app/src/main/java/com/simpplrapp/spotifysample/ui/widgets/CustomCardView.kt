package com.simpplrapp.spotifysample.ui.widgets

import android.content.Context
import android.os.Build
import android.support.v7.widget.CardView
import android.util.AttributeSet

class CustomCardView : CardView {

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (Build.VERSION.SDK_INT < 21)
            radius = 0f
    }
}
