package com.simpplrapp.spotifysample.ui.widgets;

import android.content.Context;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by avninder.singh on 24/03/18.
 */


public class EndlessRecyclerView extends RecyclerView {

    /**
     * For RecyclerView Endless scrolling
     */
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 5;

    private EndlessScrollListener mEndlessScrollListener;
    private DataObserver dataObserver;
    private RecyclerView.Adapter mAdapter;
    private View emptyView;

    public void setLoading(boolean b) {

        loading = false;
    }

    public interface EndlessScrollListener {
        void onNextRequest();
    }

    public EndlessRecyclerView(Context context) {
        super(context);
        init();
    }

    public EndlessRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EndlessRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        dataObserver = new DataObserver();
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
    }

    public void setVisibleThreshold(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    public void setEndlessScrollListener(EndlessScrollListener endlessScrollListener) {
        mEndlessScrollListener = endlessScrollListener;
    }

    @Override
    public void setLayoutManager(LayoutManager layout) {
        addScrollListener();
        super.setLayoutManager(layout);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        registerDataObserver();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unRegisterDataObserver();
    }

    @Override
    public void setAdapter(Adapter adapter) {
        mAdapter = adapter;
        registerDataObserver();
        super.setAdapter(adapter);
    }

    private void registerDataObserver() {
        try {


            if (mAdapter != null && !mAdapter.hasObservers()) {

                mAdapter.registerAdapterDataObserver(dataObserver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void unRegisterDataObserver() {
        try {
            if (mAdapter != null) {
                mAdapter.unregisterAdapterDataObserver(dataObserver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addScrollListener() {
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                handleScrollState(dy);
            }
        });
    }

    public void fakeScroll() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                handleScrollState(1);
            }
        }, 400);
    }

    public void reInit() {
        previousTotal = 0;
        loading = true;
    }


    public static class SavedState extends View.BaseSavedState {
        int previousTotal;
        boolean loading;
        Parcelable adapterState;
        ClassLoader loader;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(previousTotal);
            out.writeByte(loading ? (byte) 1 : (byte) 0);
            out.writeParcelable(adapterState, flags);
        }

        @Override
        public String toString() {
            return "";
        }

        public static final Creator<SavedState> CREATOR
                = ParcelableCompat.newCreator(new ParcelableCompatCreatorCallbacks<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in, ClassLoader loader) {
                return new SavedState(in, loader);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        });

        SavedState(Parcel in, ClassLoader loader) {
            super(in);
            if (loader == null) {
                loader = EndlessRecyclerView.SavedState.class.getClassLoader();
            }
            previousTotal = in.readInt();
            loading = in.readByte() != 0;
            adapterState = in.readParcelable(loader);
            this.loader = loader;
        }
    }


    /**
     * @param dy The amount of vertical scroll
     */
    private void handleScrollState(int dy) {
        LinearLayoutManager mLayoutManager = null;
        int visibleItemCount = 0, totalItemCount = 0, firstVisibleItem = 0;
        if (getLayoutManager() instanceof LinearLayoutManager || getLayoutManager() instanceof GridLayoutManager) {
            mLayoutManager = (LinearLayoutManager) getLayoutManager();
            visibleItemCount = mLayoutManager.getChildCount();
            totalItemCount = mLayoutManager.getItemCount();
            firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

        } else if (getLayoutManager() instanceof StaggeredGridLayoutManager) {

            StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager) getLayoutManager();
            visibleItemCount = layoutManager.getChildCount();
            totalItemCount = layoutManager.getItemCount();
            int firstVisibleItems[] = layoutManager.findFirstVisibleItemPositions(null);
            for (int i = 0; i < firstVisibleItems.length; i++) {
                if (firstVisibleItem < firstVisibleItems[i]) {
                    firstVisibleItem = firstVisibleItems[i];
                }
            }

        }
        if (dy > 0 && getLayoutManager() != null) {
            if (loading) {
                if (totalItemCount != previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }

            }

            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {
                if (mEndlessScrollListener != null)
                    mEndlessScrollListener.onNextRequest();
                loading = true;
            }
        } else if (mLayoutManager != null && mLayoutManager.getOrientation() == LinearLayoutManager.HORIZONTAL) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }

            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {
                if (mEndlessScrollListener != null)
                    mEndlessScrollListener.onNextRequest();
                loading = true;
            }


        }
    }

    private class DataObserver extends AdapterDataObserver {

        @Override
        public void onChanged() {
            super.onChanged();

            if (mAdapter != null) {

                final boolean hasEmptyData = mAdapter.getItemCount() == 0;

                if (!hasEmptyData) {
                    fakeScroll();
                } else {
                    reInit();
                }

                if (emptyView != null) {
                    //// TODO: 2/3/17 Do something with the empty views
                }
            }

        }
    }
}