package com.simpplrapp.spotifysample.ui.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.simpplrapp.spotifysample.R
import kotlinx.android.synthetic.main.layout_error_default_view.view.*

/**
 * Created by avninder.singh on 24/03/18.
 */


class ProgressLayout(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    private var errorView: View? = null
    private var progressView: View? = null
    private var mainView: View? = null
    private var progressListener: ProgressListener? = null

    interface ProgressListener {
        fun onProgressReloadClick()
    }

    init {
        init(context, attrs)
    }

    fun setProgressListener(progressListener: ProgressListener) {
        this.progressListener = progressListener
    }

    private fun init(context: Context, attrs: AttributeSet) {
        if (isInEditMode)
            return

        /*Inflate the main layout*/
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.layout_default_progress, this, true)

        /*Add Progress bar and shimmer container*/
        progressView = inflater.inflate(R.layout.layout_progress_default_view, this, false)
        this.addView(progressView)

        /*Add error view*/
        errorView = inflater.inflate(R.layout.layout_error_default_view, this, false)
        this.addView(errorView)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        /*Once the view is inflated, we have to get the child element defined in the xml*/
        mainView = getChildAt(2)

        if (!isInEditMode) {

            //By Default
            showProgress(false)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        showProgress(false)
    }

    private fun toggleErrorView(show: Boolean) {
        errorView?.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun toggleMainView(show: Boolean) {
        mainView?.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun toggleProgressView(show: Boolean) {
        progressView?.visibility = if (show) View.VISIBLE else View.GONE
    }


    fun showError(errorMsg: String?) {
        tv_error_title.text = errorMsg ?: ""
        showErrorView()
    }

    fun showMainView() {
        toggleMainView(true)
        toggleProgressView(false)
        toggleErrorView(false)
    }

    fun showErrorView() {
        toggleErrorView(true)
        toggleProgressView(false)
        toggleMainView(false)

        tv_retry.setOnClickListener { progressListener?.onProgressReloadClick() }
    }

    fun showProgress(show: Boolean) {
        if (show && progressView!!.visibility == View.VISIBLE) return
        if (!show && progressView!!.visibility == View.GONE) return

        toggleProgressView(show)
        toggleMainView(!show)
        toggleErrorView(false)
    }
}

