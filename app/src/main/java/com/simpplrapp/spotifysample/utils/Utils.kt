package com.simpplrapp.spotifysample.utils

import android.content.Context
import android.support.v7.widget.RecyclerView

/**
 * Created by avninder.singh on 24/03/18.
 */

fun dpResourceToPx(context: Context, dimenResourceIdentifier: Int): Int {
    return context.resources.getDimensionPixelSize(dimenResourceIdentifier)
}


fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(adapterPosition, itemViewType)
    }
    return this
}